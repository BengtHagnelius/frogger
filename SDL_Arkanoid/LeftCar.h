#pragma once
#include "IEntity.h"

class LeftCar :public IEntity
{
public:
	LeftCar(Sprite* p_pxSprite, int p_iX, int p_iY, int p_iScreenWidth, int p_iScreenHeight);
	~LeftCar();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

private:
	LeftCar() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	int m_iX;
	int m_iY;
	bool m_bVisible;

	float m_fMoveDelay;
	int m_iScreenWidth;
	int m_iScreenHeight;
};
