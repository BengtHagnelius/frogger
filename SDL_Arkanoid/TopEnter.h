#pragma once
#include "IEntity.h"
class InputManager;

class TopEnter :public IEntity
{
public:
	TopEnter(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~TopEnter();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

	float m_fX;
	float m_fY;
	bool StartGame = false;

private:
	TopEnter() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	InputManager* m_pxInput;
	bool m_bVisible;
	int m_iScreenWidth;
	int m_iScreenHeight;
};

