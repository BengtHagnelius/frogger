#include "stdafx.h"
#include "LoseState.h"
#include "Engine.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "InputManager.h"
#include "GameOverText.h"
#include "GameState.h"
#include "MenuState.h"
#include "AnimatedSprite.h"
#include "Sprite.h"

LoseState::LoseState(System& p_xSystem)
{
	m_pxGameOverText = nullptr;
	m_pxGameOverText2 = nullptr;
	m_xSystem = p_xSystem;
}

LoseState::~LoseState()
{

}

void LoseState::Enter()
{

	Mix_HaltMusic();

	m_xMusic = Mix_LoadMUS("../assets/GameOver.wav");
	if (m_xMusic == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}

	p_pxGameOverSprite = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/EndStateText.bmp");
	p_pxGameOverSprite->AddFrame(0,  0, 180, 36, 1.2f);
	p_pxGameOverSprite->AddFrame(0, 36, 180, 36, 1.2f);
	p_pxGameOverSprite->AddFrame(0, 72, 180, 36, 1.2f);
	m_pxGameOverText = new GameOverText(p_pxGameOverSprite, 170, -36,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	p_pxLoseFrog =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/EndStateText.bmp", 290, 20, 84, 84);
	m_pxLoseFrog = new GameOverText(p_pxLoseFrog, (m_xSystem.m_iScreenWidth / 2) - 42,
		(m_xSystem.m_iScreenHeight / 2) - 42,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	p_pxPressEnterSprite = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/EndStateText.bmp");
	p_pxPressEnterSprite->AddFrame(0, 216, 280, 36, 1.2f);
	p_pxPressEnterSprite->AddFrame(0, 252, 280, 36, 1.2f);
	p_pxPressEnterSprite->AddFrame(0, 288, 280, 36, 1.2f);
	m_pxGameOverText2 = new GameOverText(p_pxPressEnterSprite, 120, 336,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	Mix_PlayMusic(m_xMusic, 1);
}

void LoseState::Exit()
{

}

bool LoseState::Update(float p_fDeltaTime)
{

	m_pxGameOverText->Update(p_fDeltaTime);
	m_pxGameOverText2->Update(p_fDeltaTime);

	p_pxGameOverSprite->Update(p_fDeltaTime);
	p_pxPressEnterSprite->Update(p_fDeltaTime);
	if (m_xSystem.m_pxInput->IsKeyDownOnce(SDLK_RETURN))
	{
		return false;
	}
	return true;
}

void LoseState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(p_pxGameOverSprite, m_pxGameOverText->GetX(), m_pxGameOverText->GetY());
	m_xSystem.m_pxDrawManager->Draw(p_pxLoseFrog, m_pxLoseFrog->GetX(), m_pxLoseFrog->GetY());
	m_xSystem.m_pxDrawManager->Draw(p_pxPressEnterSprite, m_pxGameOverText2->GetX(), m_pxGameOverText2->GetY());
}

IState* LoseState::NextState()
{
	return new MenuState(m_xSystem);
}

