#include "stdafx.h"
#include "LogoBottom.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Collider.h"


LogoBottom::LogoBottom(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxInput = p_pxInput;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}


LogoBottom::~LogoBottom()
{
}

void LogoBottom::Update(float p_fDeltaTime)
{
	if (StartGame)
	{
		m_fX += 1.0f;
	}
}

Sprite* LogoBottom::GetSprite()
{
	return m_pxSprite;
}

Collider* LogoBottom::GetCollider()
{
	return m_pxCollider;
}

float LogoBottom::GetX() { return m_fX; }

float LogoBottom::GetY() { return m_fY; }

bool LogoBottom::IsVisible() { return m_bVisible; }

EENTITYTYPE LogoBottom::GetType() { return EENTITYTYPE::ENTITY_LOGOBOTTOM; }