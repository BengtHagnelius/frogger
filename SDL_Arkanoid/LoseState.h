#pragma once
#include "IState.h"

class GameOverText;
class AnimatedSprite;
class Sprite;

class LoseState : public IState
{
public:
	LoseState(System& p_xSystem);
	~LoseState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();

private:
	System m_xSystem;

	Mix_Music* m_xMusic;

	GameOverText* m_pxGameOverText;
	GameOverText* m_pxGameOverText2;
	GameOverText* m_pxLoseFrog;

	Sprite* p_pxLoseFrog;
	
	AnimatedSprite* p_pxGameOverSprite;
	AnimatedSprite* p_pxPressEnterSprite;

};

