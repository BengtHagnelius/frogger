#pragma once
#include "IEntity.h"

class Player;

class RightTurtle :public IEntity
{
public:
	RightTurtle(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~RightTurtle();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

	float m_fMoveDelay;

private:
	RightTurtle() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	Player* m_pxPlayer;
	float m_fX;
	float m_fY;
	bool m_bVisible;
	int m_iScreenWidth;
	int m_iScreenHeight;
};
