#include "stdafx.h"
#include "LeftCar.h"
#include "Sprite.h"
#include "Collider.h"

LeftCar::LeftCar(Sprite* p_pxSprite, int p_iX, int p_iY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_iX = p_iX;
	m_iY = p_iY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}

LeftCar::~LeftCar()
{

}

void LeftCar::Update(float p_fDeltaTime)
{
	m_iX --;
	if (m_iX < -50)
	{
		m_iX = 520;
	}
	m_pxCollider->Refresh();
}

Sprite* LeftCar::GetSprite()
{
	return m_pxSprite;
}

Collider* LeftCar::GetCollider()
{
	return m_pxCollider;
}

float LeftCar::GetX() { return m_iX; }

float LeftCar::GetY() { return m_iY; }

bool LeftCar::IsVisible() { return m_bVisible; }

EENTITYTYPE LeftCar::GetType() { return EENTITYTYPE::ENTITY_LEFTCAR; }
