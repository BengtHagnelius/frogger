#include "stdafx.h"
#include "Goal.h"
#include "Sprite.h"
#include "Collider.h"

Goal::Goal(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}

Goal::~Goal()
{

}

void Goal::Update(float p_fDeltaTime)
{

}

Sprite* Goal::GetSprite()
{
	return m_pxSprite;
}

Collider* Goal::GetCollider()
{
	return m_pxCollider;
}

float Goal::GetX() { return m_fX; }

float Goal::GetY() { return m_fY; }

bool Goal::IsVisible() { return m_bVisible; }

EENTITYTYPE Goal::GetType() { return EENTITYTYPE::ENTITY_GOAL; }

void Goal::SetVisible(bool p_bValue)
{
	m_bVisible = p_bValue;
}