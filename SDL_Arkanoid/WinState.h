#pragma once
#include "IState.h"

class WinText;
class Sprite;
class AnimatedSprite;

class WinState : public IState
{
public:
	WinState(System& p_xSystem);
	~WinState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();

private:
	System m_xSystem;

	Mix_Music* m_xMusic;

	WinText* m_pxWinText;
	WinText* m_pxWinText2;
	WinText* m_pxWinFrog;

	Sprite* p_pxWinFrog;

	AnimatedSprite* p_pxWinSprite;
	AnimatedSprite* p_pxPressEnterSprite;

};

