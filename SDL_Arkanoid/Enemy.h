#pragma once

#include "IEntity.h"

class Enemy : public IEntity
{
public:
	Enemy(Sprite* pxEnemySprite,
		int p_fX, int p_fY, int p_iScreenHeight, int p_iScreenWidth);
	~Enemy();
	void Update(float p_fDeltaTime);
	void WalkUp();
	void WalkDown();
	void WalkLeft();
	void WalkRight();
	void Shoot();
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	void SetPosition(float p_pX, float p_fY);
	EENTITYTYPE GetType();

private: 
	Enemy() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;

	int m_fX;
	int m_fY;
	int m_iScreenHeight;
	int m_iScreenWidth;
	bool m_bVisible;
	float fireDelay;
};