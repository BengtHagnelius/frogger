#pragma once

/*
	Suggested structure for a potential future InputManager
	Init will create objects needed and shutdown will delete them.
	Update will handle all the events with SDL_PollEvent update the 
	mouse and keyboard objects accordingly.
	Other functions will return appropriate data from the mouse and keyboard.
*/

//Fixa InputManager

class InputManager
{
public:
	InputManager();
	~InputManager();
	void Initialize();
	void Shutdown();
	bool IsKeyDown(int p_iIndex);
	bool IsKeyDownOnce(int p_iIndex);
	void SetKey(int p_iIndex, bool p_bValue);
	void Update();
private:
	SDL_Event xEvent;

	bool KeyDown;
	bool m_abKeys[256];
};