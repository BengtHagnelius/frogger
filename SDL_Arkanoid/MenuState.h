#pragma once
#include "IState.h"

class LogoTop;
class LogoBottom;
class Sprite;
class AnimatedSprite;
class BottomEnter;
class TopEnter;
class BottomEnter;
class IState;
class InputManager;

class MenuState : public IState
{
public:
	MenuState(System& p_xSystem);
	~MenuState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();

private:
	InputManager* m_pxInput;

	Mix_Music* m_xMusic;

	LogoTop* m_pxTopLogo;
	LogoBottom* m_pxBottomLogo;
	TopEnter* m_pxTopEnter;
	BottomEnter* m_pxBottomEnter;

	Sprite* m_pxBackground;
	Sprite* m_pxFroggerLogoTopSprite;
	Sprite* m_pxFroggerLogoBottomSprite;

	AnimatedSprite* m_pxTopEnterSprite;
	AnimatedSprite* m_pxBottomEnterSprite;

	Mix_Music* m_xMenuMusic;

	System m_xSystem;
};