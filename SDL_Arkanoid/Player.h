#pragma once

#include "IEntity.h"
class InputManager;

class Player : public IEntity
{
public:
	Player(InputManager* p_pxInput, Sprite* pxPlayerSprite,
		float p_fX, float p_fY, int p_iScreenHeight, int p_iScreenWidth);
	~Player();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	void SetPosition(float p_fX, float p_fY);
	EENTITYTYPE GetType();
	float m_fX;
	float m_fY;
	int m_iLives = 4;
	bool m_bEnd = false;
	bool m_bGameStart = false;
	int m_iWDir = 1;
	

private:
	Player() {};
	Mix_Chunk* m_xPlopSound;
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	InputManager* m_pxInput;
	bool m_bVisible;
	int m_iScreenHeight;
	int m_iScreenWidth;
};