#include "stdafx.h"
#include "RightTurtle.h"
#include "Player.h"
#include "AnimatedSprite.h"
#include "Sprite.h"
#include "Collider.h"

RightTurtle::RightTurtle(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_bVisible = true;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
	m_fMoveDelay = 0.0f;
}

RightTurtle::~RightTurtle()
{

}

void RightTurtle::Update(float p_fDeltaTime)
{
	m_fX += 0.8f;

	if (m_fX > 520)
	{
		m_fX = -100;
	}
	m_pxCollider->Refresh();
}

Sprite* RightTurtle::GetSprite()
{
	return m_pxSprite;
}

Collider* RightTurtle::GetCollider()
{
	return m_pxCollider;
}

float RightTurtle::GetX() { return m_fX; }

float RightTurtle::GetY() { return m_fY; }

bool RightTurtle::IsVisible() { return m_bVisible; }

EENTITYTYPE RightTurtle::GetType() { return EENTITYTYPE::ENTITY_RIGHTTURTLE; }
