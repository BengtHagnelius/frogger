#pragma once

#include "IState.h"
class Player;
class RightLog;
class LeftLog;
class InputManager;
class Sprite;
class AnimatedSprite;
class LeftCar;
class RightCar;
class LeftTurtle;
class RightTurtle;
class Goal;

class GameState : public IState
{
public:
	GameState(System& p_xSystem);
	~GameState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();

	
private:
	void CheckCollision();
	System m_xSystem;

	Mix_Music* m_xMusic;
	Mix_Chunk* m_xDeathSound;
	Mix_Chunk* m_xPointSound;

	Player* m_pxPlayer;

	Goal* m_pxGoal;
	Goal* m_pxGoal1;
	Goal* m_pxGoal2;
	Goal* m_pxGoal3;
	Goal* m_pxGoal4;

	RightTurtle* m_pxRightTurtle;
	RightTurtle* m_pxRightTurtle1;
	RightTurtle* m_pxRightTurtle2;
	RightTurtle* m_pxRightTurtle3;
	LeftTurtle* m_pxLeftTurtle;
	LeftTurtle* m_pxLeftTurtle1;
	LeftTurtle* m_pxLeftTurtle2;
	LeftTurtle* m_pxLeftTurtle3;

	RightLog* m_pxRightLog;
	RightLog* m_pxRightLog1;
	RightLog* m_pxRightLog2;
	RightLog* m_pxRightLog3;
	RightLog* m_pxRightLog4;
	RightLog* m_pxRightLog5;
	RightLog* m_pxRightLog6;
	RightLog* m_pxRightLog7;  

	LeftLog* m_pxLeftLog;
	LeftLog* m_pxLeftLog1;
	LeftLog* m_pxLeftLog2;
	LeftLog* m_pxLeftLog3;
	LeftLog* m_pxLeftLog4;
	LeftLog* m_pxLeftLog5;
	LeftLog* m_pxLeftLog6;
	LeftLog* m_pxLeftLog7;

	LeftCar* m_pxLeftCar;
	LeftCar* m_pxLeftCar1;
	LeftCar* m_pxLeftCar2;
	LeftCar* m_pxLeftCar3;
	LeftCar* m_pxLeftCar4;
	LeftCar* m_pxLeftCar5;
	LeftCar* m_pxLeftCar6;
	LeftCar* m_pxLeftCar7;
	LeftCar* m_pxLeftTruck;
	LeftCar* m_pxLeftTruck1;
	LeftCar* m_pxLeftTruck2;

	RightCar* m_pxRightCar;
	RightCar* m_pxRightCar1;
	RightCar* m_pxRightCar2;
	RightCar* m_pxRightCar3;
	RightCar* m_pxRightTruck;
	RightCar* m_pxRightTruck1;
	RightCar* m_pxRightTruck2;

	AnimatedSprite* pxRTurtleSprite;
	AnimatedSprite* pxLTurtleSprite;

	Sprite* pxPlayerSprite;
	Sprite* pxPlayerSpriteLeft;
	Sprite* pxPlayerSpriteRight;
	Sprite* pxPlayerSpriteDown;

	Sprite* p_pxBackground;
	Sprite* p_pxLifeSprite;
	Sprite* p_pxGoalSprite;
	Sprite* p_pxLogSprite3;
	Sprite* p_pxLogSprite4;
	Sprite* p_pxLogSprite5;
	Sprite* p_pxLeftTruck;
	Sprite* p_pxLeftCar;
	Sprite* p_pxLeftCar1;
	Sprite* p_pxLeftCar2;
	Sprite* p_pxRightTruck;
	Sprite* p_pxRightCar;

	int moveDelay;
	bool m_bGameRunning;
	int m_iGoalsLeft;
};
