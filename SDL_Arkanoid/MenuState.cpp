#include "stdafx.h"
#include "MenuState.h"
#include "Engine.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Sprite.h"
#include "LogoTop.h"
#include "LogoBottom.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "BottomEnter.h"
#include "TopEnter.h"
#include "GameState.h"
#include "WinState.h"

MenuState::MenuState(System & p_xSystem)
{
	m_pxTopLogo = nullptr;
	m_pxBottomLogo = nullptr;
	m_xMenuMusic = nullptr;
	m_xSystem = p_xSystem;

	m_xMusic = nullptr;

	Mix_Init(MIX_INIT_MP3);
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 4, 1024) == -1)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}

	m_xMusic = Mix_LoadMUS("../assets/FroggerlMusicLoop.wav");
	if (m_xMusic == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}
}

MenuState::~MenuState()
{
	
}

void MenuState::Enter()
{
	//Ladda in bakgrund
	m_pxBackground = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerMenuBackgroundSprite.bmp", 0, 0, 520, 300);
	//Ladda in logga
	m_pxFroggerLogoTopSprite = 
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerLogo.bmp", 0, 0, 317, 15);
		m_pxTopLogo = new LogoTop(m_xSystem.m_pxInput, m_pxFroggerLogoTopSprite, 260 - 317/2, 85,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	m_pxFroggerLogoBottomSprite =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerLogo.bmp", 0, 15, 317, 15);
		m_pxBottomLogo = new LogoBottom(m_xSystem.m_pxInput, m_pxFroggerLogoBottomSprite, 260 - 317/2, 105,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	m_pxTopEnterSprite =
		m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/PressToStart.bmp");
		m_pxTopEnterSprite->AddFrame(0,  0, 220, 18, 0.5f);
		m_pxTopEnterSprite->AddFrame(0, 36, 220, 18, 0.5f);
		m_pxTopEnterSprite->AddFrame(0, 72, 220, 18, 0.5f);
		m_pxTopEnter = new TopEnter(m_xSystem.m_pxInput, m_pxFroggerLogoBottomSprite, 150, 201,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	m_pxBottomEnterSprite =
		m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/PressToStart.bmp");
		m_pxBottomEnterSprite->AddFrame(0, 18, 160, 18, 0.5f);
		m_pxBottomEnterSprite->AddFrame(0, 54, 160, 18, 0.5f);
		m_pxBottomEnterSprite->AddFrame(0, 90, 160, 18, 0.5f);
		m_pxBottomEnter = new BottomEnter(m_xSystem.m_pxInput, m_pxFroggerLogoBottomSprite, 180, 221,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	//Ladda in musik
		Mix_PlayMusic(m_xMusic, -1);
}

void MenuState::Exit()
{
	//Nulla pekare h�r
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxFroggerLogoTopSprite);
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxFroggerLogoBottomSprite);
	//Anv�nd spritemanagerns destroysprite
}

bool MenuState::Update(float p_fDeltaTime)
{

	if (m_xSystem.m_pxInput->IsKeyDownOnce(SDLK_RETURN))
	{
		m_pxBottomLogo->StartGame = true;
		m_pxTopLogo->StartGame = true;
		m_pxTopEnter->StartGame = true;
		m_pxBottomEnter->StartGame = true;
	}
	if (m_xSystem.m_pxInput->IsKeyDownOnce(SDLK_ESCAPE))
	{
		SDL_Quit;
	}

	m_pxBottomLogo->Update(p_fDeltaTime);
	m_pxTopLogo->Update(p_fDeltaTime);
	m_pxTopEnter->Update(p_fDeltaTime);
	m_pxBottomEnter->Update(p_fDeltaTime);
	m_pxTopEnterSprite->Update(p_fDeltaTime);
	m_pxBottomEnterSprite->Update(p_fDeltaTime);

	if (m_pxTopEnter->GetX() > 520)
	{
		return false;
	}

	return true;
}

void MenuState::Draw()
{
	//rita ut sprites
	m_xSystem.m_pxDrawManager->Draw(m_pxBackground, 0, 0);
	m_xSystem.m_pxDrawManager->Draw(m_pxFroggerLogoTopSprite, m_pxTopLogo->GetX(), 85);
	m_xSystem.m_pxDrawManager->Draw(m_pxFroggerLogoBottomSprite, m_pxBottomLogo->GetX(), 100);
	m_xSystem.m_pxDrawManager->Draw(m_pxTopEnterSprite, m_pxTopEnter->GetX(), 201);
	m_xSystem.m_pxDrawManager->Draw(m_pxBottomEnterSprite, m_pxBottomEnter->GetX(), 221);
	
}

IState* MenuState::NextState()
{
	return new WinState(m_xSystem);
}
