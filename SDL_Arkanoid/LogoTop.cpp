#include "stdafx.h"
#include "LogoTop.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Collider.h"

LogoTop::LogoTop(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxInput = p_pxInput;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}


LogoTop::~LogoTop()
{
}

void LogoTop::Update(float p_fDeltaTime)
{
	if (StartGame)
	{
		m_fX -= 1.0f;
	}
}

Sprite* LogoTop::GetSprite()
{
	return m_pxSprite;
}

Collider* LogoTop::GetCollider()
{
	return m_pxCollider;
}

float LogoTop::GetX() { return m_fX; }

float LogoTop::GetY() { return m_fY; }

bool LogoTop::IsVisible() { return m_bVisible; }

EENTITYTYPE LogoTop::GetType() { return EENTITYTYPE::ENTITY_LOGOTOP; }
