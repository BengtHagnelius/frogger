#pragma once
#include "IEntity.h"

class LeftLog :public IEntity
{
public:
	LeftLog(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~LeftLog();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

private:
	LeftLog() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	float m_fX;
	float m_fY;
	bool m_bVisible;

	float m_fMoveDelay;
	int m_iScreenWidth;
	int m_iScreenHeight;
};
