#include "stdafx.h"
#include "RightCar.h"
#include "Sprite.h"
#include "Collider.h"

RightCar::RightCar(Sprite* p_pxSprite, int p_iX, int p_iY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_iX = p_iX;
	m_iY = p_iY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
	m_fMoveDelay = 0.0f;
}

RightCar::~RightCar()
{

}

void RightCar::Update(float p_fDeltaTime)
{
	m_iX++;
	/*m_fMoveDelay += p_fDeltaTime;
	if (m_fMoveDelay > 0.75f)
	{
	m_iX -= 20;
	m_fMoveDelay = 0;
	}
	*/
	if (m_iX > 570)
	{
		m_iX = -50;
	}
	m_pxCollider->Refresh();
}

Sprite* RightCar::GetSprite()
{
	return m_pxSprite;
}

Collider* RightCar::GetCollider()
{
	return m_pxCollider;
}

float RightCar::GetX() { return m_iX; }

float RightCar::GetY() { return m_iY; }

bool RightCar::IsVisible() { return m_bVisible; }

EENTITYTYPE RightCar::GetType() { return EENTITYTYPE::ENTITY_RIGHTCAR; }
