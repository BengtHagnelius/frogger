#pragma once
#include "IEntity.h"
class InputManager;

class LogoTop:public IEntity
{
public:
	LogoTop(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~LogoTop();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	
	float m_fX;
	float m_fY;
	bool StartGame = false;

private:
	LogoTop() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	InputManager* m_pxInput;
	bool m_bVisible;
	int m_iScreenWidth;
	int m_iScreenHeight;
};

