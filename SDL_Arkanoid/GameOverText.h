#pragma once
#include "IEntity.h"
class GameOverText : public IEntity
{
public:
	GameOverText(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenHeight, int p_iScreenWidth);
	~GameOverText();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

private:
	GameOverText() {};
	float m_fX;
	float m_fY;
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	bool m_bVisible;
	int m_iScreenWidth;
	int m_iScreenHeight;
};

