#pragma once
#include "IState.h"
#include "stdafx.h"

class EndState
{
public:
	EndState();
	~EndState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();
};

