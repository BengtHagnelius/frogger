#include "stdafx.h"
#include "BottomEnter.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Collider.h"

BottomEnter::BottomEnter(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{

	m_pxInput = p_pxInput;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}


BottomEnter::~BottomEnter()
{
}

void BottomEnter::Update(float p_fDeltaTime)
{
	if (StartGame)
	{
		m_fX -= 1.0f;
	}
}

Sprite* BottomEnter::GetSprite()
{
	return m_pxSprite;
}

Collider* BottomEnter::GetCollider()
{
	return m_pxCollider;
}

float BottomEnter::GetX() { return m_fX; }

float BottomEnter::GetY() { return m_fY; }

bool BottomEnter::IsVisible() { return m_bVisible; }

EENTITYTYPE BottomEnter::GetType() { return EENTITYTYPE::ENTITY_BOTTOMENTER; }
