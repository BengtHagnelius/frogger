#include "stdafx.h"
#include "GameState.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Mouse.h"
#include "Sprite.h"
#include "Player.h"
#include "RightLog.h"
#include "LeftLog.h"
#include "CollisionManager.h"
#include <fstream>
#include <string>
#include <iostream>
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "LeftCar.h"
#include "RightCar.h"
#include "RightTurtle.h"
#include "LeftCar.h"
#include "LeftTurtle.h"
#include "Goal.h"
#include "LoseState.h"
#include "WinState.h"

GameState::GameState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_pxPlayer = nullptr;
	m_pxRightLog = nullptr;
	m_pxRightLog1 = nullptr;
	m_pxRightLog2 = nullptr;
	m_pxRightLog3 = nullptr;
	m_pxRightLog4 = nullptr;
	m_pxRightLog5 = nullptr;
	m_pxRightLog6 = nullptr;
	m_pxRightLog7 = nullptr;
	m_pxLeftLog = nullptr;
	m_pxLeftLog1 = nullptr;
	m_pxLeftLog2 = nullptr;
	m_pxLeftLog3 = nullptr;
	m_pxLeftLog4 = nullptr;
	m_pxLeftLog5 = nullptr;
	m_pxLeftLog6 = nullptr;
	m_pxLeftLog7 = nullptr;

	m_iGoalsLeft = 5;

	m_xDeathSound = nullptr;
	m_xPointSound = nullptr;

	m_xDeathSound = Mix_LoadWAV("../assets/Death.wav");
	if (m_xDeathSound == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}

	m_xPointSound = Mix_LoadWAV("../assets/Point.wav");
	if (m_xPointSound == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}


}

GameState::~GameState()
{
	
}

void GameState::Enter()
{
	//TODO: SoundManager
	p_pxBackground =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerBackgroundSprite.bmp", 0, 0, 520, 300); //Draws a background


	pxPlayerSprite =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 20, 249, 20, 20);
	pxPlayerSpriteLeft =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 40, 249, 20, 20);
	pxPlayerSpriteRight =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 249, 20, 20);
	pxPlayerSpriteDown =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 60, 249, 20, 20);
	SDL_Rect* xRect = pxPlayerSprite->GetRegion();
	int iHeight = xRect->h;
	int iWidth = xRect->w;
	m_pxPlayer = new Player(m_xSystem.m_pxInput, pxPlayerSprite, m_xSystem.m_iScreenWidth / 2,
		m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2),
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	pxRTurtleSprite =
		m_xSystem.m_pxSpriteManager->
		CreateAnimatedSprite("../assets/FroggerSprites.bmp");
	pxRTurtleSprite->AddFrame(70, 135, 65, 20, 0.5f);
	pxRTurtleSprite->AddFrame(70, 155, 65, 20, 0.5f);
	m_pxRightTurtle = new RightTurtle(
		pxRTurtleSprite, -545, 60,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect35 = pxRTurtleSprite->GetRegion();

	m_pxRightTurtle1 = new RightTurtle(
		pxRTurtleSprite, -385, 60,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect36 = pxRTurtleSprite->GetRegion();

	m_pxRightTurtle2 = new RightTurtle(
		pxRTurtleSprite, -225, 60,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect37 = pxRTurtleSprite->GetRegion();

	m_pxRightTurtle3 = new RightTurtle(
		pxRTurtleSprite, -65, 60,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect38 = pxRTurtleSprite->GetRegion();

	pxLTurtleSprite =
		m_xSystem.m_pxSpriteManager->
		CreateAnimatedSprite("../assets/FroggerSprites.bmp");
	pxLTurtleSprite->AddFrame(0, 135, 65, 20, 0.5f);
	pxLTurtleSprite->AddFrame(0, 155, 65, 20, 0.5f);
	m_pxLeftTurtle = new LeftTurtle(
		pxRTurtleSprite, 520, 120,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect39 = pxRTurtleSprite->GetRegion();

	m_pxLeftTurtle1 = new LeftTurtle(
		pxRTurtleSprite, 680, 120,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect40 = pxRTurtleSprite->GetRegion();

	m_pxLeftTurtle2 = new LeftTurtle(
		pxRTurtleSprite, 840, 120,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect41 = pxRTurtleSprite->GetRegion();

	m_pxLeftTurtle3 = new LeftTurtle(
		pxRTurtleSprite, 1000, 120,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect42 = pxRTurtleSprite->GetRegion();


	p_pxGoalSprite =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 150, 0, 40, 20);

	m_pxGoal = new Goal(p_pxGoalSprite, 40, 20, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect43 = p_pxGoalSprite->GetRegion();

	m_pxGoal1 = new Goal(p_pxGoalSprite, 140, 20, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect44 = p_pxGoalSprite->GetRegion();

	m_pxGoal2 = new Goal(p_pxGoalSprite, 240, 20, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect45 = p_pxGoalSprite->GetRegion();

	m_pxGoal3 = new Goal(p_pxGoalSprite, 340, 20, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect46 = p_pxGoalSprite->GetRegion();

	m_pxGoal4 = new Goal(p_pxGoalSprite, 440, 20, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect47 = p_pxGoalSprite->GetRegion();



	p_pxLeftTruck =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 61, 50, 18);
	p_pxLeftCar =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 80, 29, 18);
	p_pxLeftCar1=
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 99, 29, 18);


	m_pxLeftTruck = new LeftCar(p_pxLeftTruck, 520, 181, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect1 = p_pxLeftTruck->GetRegion();

	m_pxLeftTruck1 = new LeftCar(p_pxLeftTruck, 710, 181, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect2 = p_pxLeftTruck->GetRegion();

	m_pxLeftTruck2 = new LeftCar(p_pxLeftTruck, 900, 181, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect3 = p_pxLeftTruck->GetRegion();


	m_pxLeftCar = new LeftCar(p_pxLeftCar, 520, 221, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect4 = p_pxLeftCar->GetRegion();

	m_pxLeftCar1 = new LeftCar(p_pxLeftCar, 660, 221, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect5 = p_pxLeftCar->GetRegion();

	m_pxLeftCar2 = new LeftCar(p_pxLeftCar, 800, 221, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect6 = p_pxLeftCar->GetRegion();

	m_pxLeftCar3 = new LeftCar(p_pxLeftCar, 940, 221, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect7 = p_pxLeftCar->GetRegion();



	m_pxLeftCar4 = new LeftCar(p_pxLeftCar1, 600, 261, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect8 = p_pxLeftCar->GetRegion();

	m_pxLeftCar5 = new LeftCar(p_pxLeftCar1, 740, 261, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect9 = p_pxLeftCar->GetRegion();

	m_pxLeftCar6 = new LeftCar(p_pxLeftCar1, 880, 261, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect10 = p_pxLeftCar->GetRegion();

	m_pxLeftCar7 = new LeftCar(p_pxLeftCar1, 1020, 261, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect11 = p_pxLeftCar->GetRegion();


	p_pxRightTruck =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 48, 61, 50, 18);
	p_pxRightCar =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 26, 117, 26, 18);

	m_pxRightTruck = new RightCar(p_pxRightTruck, -50, 241, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect12 = p_pxRightTruck->GetRegion();

	m_pxRightTruck1 = new RightCar(p_pxRightTruck, -240, 241, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect13 = p_pxRightTruck->GetRegion();

	m_pxRightTruck2 = new RightCar(p_pxRightTruck, -430, 241, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect14 = p_pxRightTruck->GetRegion();



	m_pxRightCar = new RightCar(p_pxRightCar, -26, 201, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect15 = p_pxRightCar->GetRegion();

	m_pxRightCar1 = new RightCar(p_pxRightCar, -166, 201, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect16 = p_pxRightCar->GetRegion();

	m_pxRightCar2 = new RightCar(p_pxRightCar, -306, 201, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect17 = p_pxRightCar->GetRegion();

	m_pxRightCar3 = new RightCar(p_pxRightCar, -446, 201, m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect18 = p_pxRightCar->GetRegion();


	p_pxLogSprite3 =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 40, 60, 20);
	p_pxLogSprite4 = 
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 20, 80, 20);
	p_pxLogSprite5 =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 0, 100, 20);

	m_pxRightLog = new RightLog(p_pxLogSprite3,
		-580, 140,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect19 = p_pxLogSprite3->GetRegion();

	m_pxRightLog1 = new RightLog(p_pxLogSprite4,
		-420, 140,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect20 = p_pxLogSprite4->GetRegion();

	m_pxRightLog2 = new RightLog(p_pxLogSprite3,
		-260, 140,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect21 = p_pxLogSprite3->GetRegion();

	m_pxRightLog3 = new RightLog(p_pxLogSprite5,
		-100, 140,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect22 = p_pxLogSprite5->GetRegion();


	m_pxRightLog4 = new RightLog(p_pxLogSprite3,
		-580, 100,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect23 = p_pxLogSprite3->GetRegion();

	m_pxRightLog5 = new RightLog(p_pxLogSprite4,
		-420, 100,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect24 = p_pxLogSprite4->GetRegion();
		
	m_pxRightLog6 = new RightLog(p_pxLogSprite3,
		-260, 100,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect25 = p_pxLogSprite3->GetRegion();

	m_pxRightLog7 = new RightLog(p_pxLogSprite3,
		-60, 100,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect26 = p_pxLogSprite3->GetRegion();



	m_pxLeftLog = new LeftLog(p_pxLogSprite3,
		520, 80,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect27 = p_pxLogSprite3->GetRegion();

	m_pxLeftLog1 = new LeftLog(p_pxLogSprite4,
		680, 80,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect28 = p_pxLogSprite4->GetRegion();

	m_pxLeftLog2 = new LeftLog(p_pxLogSprite3,
		840, 80,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect29 = p_pxLogSprite3->GetRegion();

	m_pxLeftLog3 = new LeftLog(p_pxLogSprite5,
		1000, 80,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect30 = p_pxLogSprite5->GetRegion();


	m_pxLeftLog4 = new LeftLog(p_pxLogSprite3,
		520, 40,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect31 = p_pxLogSprite3->GetRegion();

	m_pxLeftLog5 = new LeftLog(p_pxLogSprite4,
		680, 40,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect32 = p_pxLogSprite4->GetRegion();

	m_pxLeftLog6 = new LeftLog(p_pxLogSprite3,
		840, 40,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect33 = p_pxLogSprite3->GetRegion();

	m_pxLeftLog7 = new LeftLog(p_pxLogSprite3,
		1000, 40,
		m_xSystem.m_iScreenHeight,
		m_xSystem.m_iScreenWidth);
	SDL_Rect* xRect34 = p_pxLogSprite3->GetRegion();

	p_pxLifeSprite =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/FroggerSprites.bmp", 0, 270, 20, 20);


}


void GameState::Exit()
{
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxRightLog->GetSprite());
	delete m_pxRightLog;
	m_pxRightLog = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxPlayer->GetSprite());
	delete m_pxPlayer;
	m_pxPlayer = nullptr;

}


bool GameState::Update(float p_fDeltaTime)
{
	m_pxPlayer->Update(p_fDeltaTime);

	pxRTurtleSprite->Update(p_fDeltaTime);
	m_pxRightTurtle->Update(p_fDeltaTime);
	m_pxRightTurtle1->Update(p_fDeltaTime);
	m_pxRightTurtle2->Update(p_fDeltaTime);
	m_pxRightTurtle3->Update(p_fDeltaTime);

	pxLTurtleSprite->Update(p_fDeltaTime);
	m_pxLeftTurtle->Update(p_fDeltaTime);
	m_pxLeftTurtle1->Update(p_fDeltaTime);
	m_pxLeftTurtle2->Update(p_fDeltaTime);
	m_pxLeftTurtle3->Update(p_fDeltaTime);


	m_pxRightLog->Update(p_fDeltaTime);
	m_pxRightLog1->Update(p_fDeltaTime);
	m_pxRightLog2->Update(p_fDeltaTime);
	m_pxRightLog3->Update(p_fDeltaTime);

	m_pxRightLog4->Update(p_fDeltaTime);
	m_pxRightLog5->Update(p_fDeltaTime);
	m_pxRightLog6->Update(p_fDeltaTime);
	m_pxRightLog7->Update(p_fDeltaTime);


	m_pxLeftLog->Update(p_fDeltaTime);
	m_pxLeftLog1->Update(p_fDeltaTime);
	m_pxLeftLog2->Update(p_fDeltaTime);
	m_pxLeftLog3->Update(p_fDeltaTime);

	m_pxLeftLog4->Update(p_fDeltaTime);
	m_pxLeftLog5->Update(p_fDeltaTime);
	m_pxLeftLog6->Update(p_fDeltaTime);
	m_pxLeftLog7->Update(p_fDeltaTime);


	m_pxLeftTruck->Update(p_fDeltaTime);
	m_pxLeftTruck1->Update(p_fDeltaTime);
	m_pxLeftTruck2->Update(p_fDeltaTime);

	m_pxLeftCar->Update(p_fDeltaTime);
	m_pxLeftCar1->Update(p_fDeltaTime);
	m_pxLeftCar2->Update(p_fDeltaTime);
	m_pxLeftCar3->Update(p_fDeltaTime);

	m_pxLeftCar4->Update(p_fDeltaTime);
	m_pxLeftCar5->Update(p_fDeltaTime);
	m_pxLeftCar6->Update(p_fDeltaTime);
	m_pxLeftCar7->Update(p_fDeltaTime);

	m_pxRightTruck->Update(p_fDeltaTime);
	m_pxRightTruck1->Update(p_fDeltaTime);
	m_pxRightTruck2->Update(p_fDeltaTime);

	m_pxRightCar->Update(p_fDeltaTime);
	m_pxRightCar1->Update(p_fDeltaTime);
	m_pxRightCar2->Update(p_fDeltaTime);
	m_pxRightCar3->Update(p_fDeltaTime);

	if (m_pxPlayer->m_iLives == 0)
	{
		m_pxPlayer->m_bEnd = true;
		return false;
	}
	if (m_iGoalsLeft <= 0)
	{
		m_pxPlayer->m_bEnd = true;
		return false;
	}
	if (m_pxRightLog7->GetX() > 260)
	{
		m_pxPlayer->m_bGameStart = true;
	}


	// Moved the collision code to separate it from our Update code.
	CheckCollision();
	return true;
}

void GameState::Draw()
{
	//m_xSystem.m_pxDrawManager->Draw(pxEnemySprite, m_pxEnemy->GetX(), m_pxEnemy->GetY());
	m_xSystem.m_pxDrawManager->Draw(p_pxBackground, 0, 0);


	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxRightLog->GetX(), 140);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite4, m_pxRightLog1->GetX(), 140);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxRightLog2->GetX(), 140);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite5, m_pxRightLog3->GetX(), 140);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxRightLog4->GetX(), 100);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite4, m_pxRightLog5->GetX(), 100);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxRightLog6->GetX(), 100);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxRightLog7->GetX(), 100);


	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxLeftLog->GetX(), 80);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite4, m_pxLeftLog1->GetX(), 80);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxLeftLog2->GetX(), 80);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite5, m_pxLeftLog3->GetX(), 80);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxLeftLog4->GetX(), 40);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite4, m_pxLeftLog5->GetX(), 40);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxLeftLog6->GetX(), 40);
	m_xSystem.m_pxDrawManager->Draw(p_pxLogSprite3, m_pxLeftLog7->GetX(), 40);


	m_xSystem.m_pxDrawManager->Draw(pxRTurtleSprite, m_pxRightTurtle->GetX(), 60);
	m_xSystem.m_pxDrawManager->Draw(pxRTurtleSprite, m_pxRightTurtle1->GetX(), 60);
	m_xSystem.m_pxDrawManager->Draw(pxRTurtleSprite, m_pxRightTurtle2->GetX(), 60);
	m_xSystem.m_pxDrawManager->Draw(pxRTurtleSprite, m_pxRightTurtle3->GetX(), 60);

	m_xSystem.m_pxDrawManager->Draw(pxLTurtleSprite, m_pxLeftTurtle->GetX(), 120);
	m_xSystem.m_pxDrawManager->Draw(pxLTurtleSprite, m_pxLeftTurtle1->GetX(), 120);
	m_xSystem.m_pxDrawManager->Draw(pxLTurtleSprite, m_pxLeftTurtle2->GetX(), 120);
	m_xSystem.m_pxDrawManager->Draw(pxLTurtleSprite, m_pxLeftTurtle3->GetX(), 120);

	if(m_pxPlayer->m_iWDir == 1)
		m_xSystem.m_pxDrawManager->Draw(pxPlayerSprite, m_pxPlayer->GetX(), m_pxPlayer->GetY());
	if (m_pxPlayer->m_iWDir == 2)
		m_xSystem.m_pxDrawManager->Draw(pxPlayerSpriteDown, m_pxPlayer->GetX(), m_pxPlayer->GetY());
	if (m_pxPlayer->m_iWDir == 3)
		m_xSystem.m_pxDrawManager->Draw(pxPlayerSpriteLeft, m_pxPlayer->GetX(), m_pxPlayer->GetY());
	if (m_pxPlayer->m_iWDir == 4)
		m_xSystem.m_pxDrawManager->Draw(pxPlayerSpriteRight, m_pxPlayer->GetX(), m_pxPlayer->GetY());


	m_xSystem.m_pxDrawManager->Draw(p_pxLeftTruck, m_pxLeftTruck->GetX(), 181);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftTruck, m_pxLeftTruck1->GetX(), 181);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftTruck, m_pxLeftTruck2->GetX(), 181);

	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar, m_pxLeftCar->GetX(), 221);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar, m_pxLeftCar1->GetX(), 221);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar, m_pxLeftCar2->GetX(), 221);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar, m_pxLeftCar3->GetX(), 221);

	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar1, m_pxLeftCar4->GetX(), 261);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar1, m_pxLeftCar5->GetX(), 261);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar1, m_pxLeftCar6->GetX(), 261);
	m_xSystem.m_pxDrawManager->Draw(p_pxLeftCar1, m_pxLeftCar7->GetX(), 261);


	m_xSystem.m_pxDrawManager->Draw(p_pxRightTruck, m_pxRightTruck->GetX(), 241);
	m_xSystem.m_pxDrawManager->Draw(p_pxRightTruck, m_pxRightTruck1->GetX(), 241);
	m_xSystem.m_pxDrawManager->Draw(p_pxRightTruck, m_pxRightTruck2->GetX(), 241);

	m_xSystem.m_pxDrawManager->Draw(p_pxRightCar, m_pxRightCar->GetX(), 201);
	m_xSystem.m_pxDrawManager->Draw(p_pxRightCar, m_pxRightCar1->GetX(), 201);
	m_xSystem.m_pxDrawManager->Draw(p_pxRightCar, m_pxRightCar2->GetX(), 201);
	m_xSystem.m_pxDrawManager->Draw(p_pxRightCar, m_pxRightCar3->GetX(), 201);

	if(m_pxGoal->IsVisible())
		m_xSystem.m_pxDrawManager->Draw(p_pxGoalSprite, 40, 20);

	if (m_pxGoal1->IsVisible())
		m_xSystem.m_pxDrawManager->Draw(p_pxGoalSprite, 140, 20);

	if (m_pxGoal2->IsVisible())
		m_xSystem.m_pxDrawManager->Draw(p_pxGoalSprite, 240, 20);

	if (m_pxGoal3->IsVisible())
		m_xSystem.m_pxDrawManager->Draw(p_pxGoalSprite, 340, 20);

	if (m_pxGoal4->IsVisible())
		m_xSystem.m_pxDrawManager->Draw(p_pxGoalSprite, 440, 20);


	for (int i = 0; i < m_pxPlayer->m_iLives-1; i++)
	{
		m_xSystem.m_pxDrawManager->Draw(p_pxLifeSprite, i*20, 300);
	}

}

IState* GameState::NextState()
{
	if (m_pxPlayer->m_iLives <= 0)
	{
		return new LoseState(m_xSystem);
	}
	if (m_pxPlayer->m_iLives > 0)
	{
		return new WinState(m_xSystem);
	}
}



void GameState::CheckCollision()
{

	int iOverlapX = 0;
	int iOverlapY = 0;

	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;

		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar1->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar2->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar3->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar4->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar5->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar6->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftCar7->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTruck->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTruck1->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTruck2->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightCar->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightCar1->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightCar2->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightCar3->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTruck->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTruck1->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTruck2->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
		m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
		m_pxPlayer->m_iLives--;
		m_pxPlayer->m_iWDir = 1;
		Mix_PlayChannel(-1, m_xDeathSound, 0);
	}
	if (m_pxPlayer->GetY() < 160)
	{
		if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX++;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog1->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX++;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog2->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX++;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog3->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX++;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog4->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog5->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog6->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightLog7->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX--;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog1->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX--;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog2->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX--;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog3->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX--;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog4->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog5->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog6->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 1.2f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftLog7->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 1.2f;
		}

		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTurtle->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTurtle1->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTurtle2->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxLeftTurtle3->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX -= 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTurtle->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTurtle1->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTurtle2->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxRightTurtle3->GetCollider(), iOverlapX, iOverlapY))
		{
			m_pxPlayer->m_fX += 0.8f;
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxGoal->GetCollider(), iOverlapX, iOverlapY))
		{
			if (m_pxGoal->IsVisible())
			{
				m_pxGoal->SetVisible(false);
				m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
				m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
				m_pxPlayer->m_iWDir = 1;
				m_iGoalsLeft--;
				Mix_PlayChannel(-1, m_xPointSound, 0);
			}
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxGoal1->GetCollider(), iOverlapX, iOverlapY))
		{
			if (m_pxGoal1->IsVisible())
			{
				m_pxGoal1->SetVisible(false);
				m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
				m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
				m_pxPlayer->m_iWDir = 1;
				m_iGoalsLeft--;
				Mix_PlayChannel(-1, m_xPointSound, 0);
			}
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxGoal2->GetCollider(), iOverlapX, iOverlapY))
		{
			if (m_pxGoal2->IsVisible())
			{
				m_pxGoal2->SetVisible(false);
				m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
				m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
				m_pxPlayer->m_iWDir = 1;
				m_iGoalsLeft--;
				Mix_PlayChannel(-1, m_xPointSound, 0);
			}
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxGoal3->GetCollider(), iOverlapX, iOverlapY))
		{
			if (m_pxGoal3->IsVisible())
			{
				m_pxGoal3->SetVisible(false);
				m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
				m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
				m_pxPlayer->m_iWDir = 1;
				m_iGoalsLeft--;
				Mix_PlayChannel(-1, m_xPointSound, 0);
			}
		}
		else if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxGoal4->GetCollider(), iOverlapX, iOverlapY))
		{
			if (m_pxGoal4->IsVisible())
			{
				m_pxGoal4->SetVisible(false);
				m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
				m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
				m_pxPlayer->m_iWDir = 1;
				m_iGoalsLeft--;
				Mix_PlayChannel(-1, m_xPointSound, 0);
			}
		}
		else
		{
			m_pxPlayer->m_fX = m_xSystem.m_iScreenWidth / 2;
			m_pxPlayer->m_fY = m_xSystem.m_iScreenHeight - (pxPlayerSprite->GetRegion()->h * 2);
			m_pxPlayer->m_iWDir = 1;
			m_pxPlayer->m_iLives--;
			Mix_PlayChannel(-1, m_xDeathSound, 0);
		}
	}
}

