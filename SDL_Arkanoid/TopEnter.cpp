#include "stdafx.h"
#include "TopEnter.h"
#include "Sprite.h"
#include "Collider.h"
#include "InputManager.h"

TopEnter::TopEnter(InputManager* p_pxInput, Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{

	m_pxInput = p_pxInput;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}


TopEnter::~TopEnter()
{
}

void TopEnter::Update(float p_fDeltaTime)
{
	if (StartGame)
	{
		m_fX += 1.0f;
	}
}

Sprite* TopEnter::GetSprite()
{
	return m_pxSprite;
}

Collider* TopEnter::GetCollider()
{
	return m_pxCollider;
}

float TopEnter::GetX() { return m_fX; }

float TopEnter::GetY() { return m_fY; }

bool TopEnter::IsVisible() { return m_bVisible; }

EENTITYTYPE TopEnter::GetType() { return EENTITYTYPE::ENTITY_TOPENTER; }
