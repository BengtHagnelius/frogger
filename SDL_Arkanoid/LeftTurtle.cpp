#include "stdafx.h"
#include "LeftTurtle.h"
#include "Player.h"
#include "AnimatedSprite.h"
#include "Sprite.h"
#include "Collider.h"

LeftTurtle::LeftTurtle(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_bVisible = true;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
	m_fMoveDelay = 0.0f;
}

LeftTurtle::~LeftTurtle()
{

}

void LeftTurtle::Update(float p_fDeltaTime)
{
	m_fX -= 0.8f;

	if (m_fX < -100)
	{
		m_fX = 520;
	}
	m_pxCollider->Refresh();
}

Sprite* LeftTurtle::GetSprite()
{
	return m_pxSprite;
}

Collider* LeftTurtle::GetCollider()
{
	return m_pxCollider;
}

float LeftTurtle::GetX() { return m_fX; }

float LeftTurtle::GetY() { return m_fY; }

bool LeftTurtle::IsVisible() { return m_bVisible; }

EENTITYTYPE LeftTurtle::GetType() { return EENTITYTYPE::ENTITY_LEFTTURTLE; }
