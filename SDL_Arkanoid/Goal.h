#pragma once
#include "IEntity.h"

class Goal :public IEntity
{
public:
	Goal(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~Goal();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	
	void SetVisible(bool p_bValue);

private:
	Goal() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	float m_fX;
	float m_fY;
	bool m_bVisible = true;
	int m_iScreenWidth;
	int m_iScreenHeight;
};
