#pragma once
#include "IEntity.h"

class Player;

class RightLog :public IEntity
{
public:
	RightLog(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~RightLog();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

	float m_fMoveDelay;

private:
	RightLog() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	Player* m_pxPlayer;
	float m_fX;
	float m_fY;
	bool m_bVisible;
	int m_iScreenWidth;
	int m_iScreenHeight;
};