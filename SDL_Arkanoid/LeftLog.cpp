#include "stdafx.h"
#include "LeftLog.h"
#include "Sprite.h"
#include "Collider.h"

LeftLog::LeftLog(Sprite* p_pxSprite, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
	m_fMoveDelay = 0.0f;
}

LeftLog::~LeftLog()
{

}

void LeftLog::Update(float p_fDeltaTime)
{
	if (m_fY == 80.0f)
	{
		m_fX -= 1.0f;
	}
	if (m_fY == 40.0f)
	{
		m_fX -= 1.2f;
	}

	if (m_fX < -100)
	{
		m_fX = 520;
	}
	/*	if (m_fMoveDelay > 0.75f)
	{
	m_iX += 20;

	m_fMoveDelay = 0;
	}

	if(m_iX > 520)
	{
	m_iX = -100;
	} */
	m_pxCollider->Refresh();
}

Sprite* LeftLog::GetSprite()
{
	return m_pxSprite;
}

Collider* LeftLog::GetCollider()
{
	return m_pxCollider;
}

float LeftLog::GetX() { return m_fX; }

float LeftLog::GetY() { return m_fY; }

bool LeftLog::IsVisible() { return m_bVisible; }

EENTITYTYPE LeftLog::GetType() { return EENTITYTYPE::ENTITY_LEFTLOG; }
