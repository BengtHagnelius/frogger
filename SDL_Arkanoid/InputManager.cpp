#include "stdafx.h"
#include "InputManager.h"




InputManager::InputManager()
{
	for (int i = 0; i < 256; i++)
	{
		m_abKeys[i] = false;
	}
	KeyDown = false;
}

InputManager::~InputManager()
{

}

void InputManager::Initialize()
{

}

void InputManager::Shutdown()
{

}

bool InputManager::IsKeyDown(int p_iIndex)
{
	if (p_iIndex < 0)
		return false;
	if (p_iIndex > 255)
		return false;

	return m_abKeys[p_iIndex];
}

bool InputManager::IsKeyDownOnce(int p_iIndex)
{
	if (!KeyDown)
	{
		if (p_iIndex < 0)
			return false;
		if (p_iIndex > 255)
			return false;

		if (m_abKeys[p_iIndex])
			KeyDown = true;

		return m_abKeys[p_iIndex];
	}
	return false;
}

void InputManager::SetKey(int p_iIndex,
	bool p_bValue)
{
	if (p_iIndex < 0)
		return;
	if (p_iIndex > 255)
		return;

	m_abKeys[p_iIndex] = p_bValue;
}

void InputManager::Update()
{
	SDL_Event xEvent;
	while (SDL_PollEvent(&xEvent))
	{
		if (xEvent.type == SDL_KEYDOWN)
		{
			SetKey(xEvent.key.keysym.sym, true);
		}
		else if (xEvent.type == SDL_KEYUP)
		{
			SetKey(xEvent.key.keysym.sym, false);
			KeyDown = false;
		}

	}
}