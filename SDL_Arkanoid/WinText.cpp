#include "stdafx.h"
#include "WinText.h"
#include "Sprite.h"
#include "Collider.h"


WinText::WinText(Sprite* p_pxSprite,
	float p_fX, float p_fY, int p_iScreenHeight, int p_iScreenWidth)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();

}

WinText::~WinText()
{

}

void WinText::Update(float p_fDeltaTime)
{
	if (m_fY<50)
		m_fY++;
	if (m_fY > 250)
		m_fY--;
}

Sprite* WinText::GetSprite()
{
	return m_pxSprite;
}

Collider* WinText::GetCollider()
{
	return m_pxCollider;
}

float WinText::GetX()
{
	return m_fX;
}

float WinText::GetY()
{
	return m_fY;
}

bool WinText::IsVisible()
{
	return m_bVisible;
}

EENTITYTYPE WinText::GetType()
{
	return EENTITYTYPE::ENTITY_WINTEXT;
}