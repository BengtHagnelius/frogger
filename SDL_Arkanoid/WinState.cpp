#include "stdafx.h"
#include "WinState.h"
#include "Engine.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Sprite.h"
#include "InputManager.h"
#include "WinText.h"
#include "GameState.h"
#include "MenuState.h"
#include "AnimatedSprite.h"

WinState::WinState(System& p_xSystem)
{
	m_pxWinText = nullptr;
	m_xSystem = p_xSystem;
}

WinState::~WinState()
{

}

void WinState::Enter()
{

	Mix_HaltMusic();

	m_xMusic = Mix_LoadMUS("../assets/VictoryFanfare.wav");
	if (m_xMusic == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}

	p_pxWinSprite = 
		m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/EndStateText.bmp");
	p_pxWinSprite->AddFrame(0, 108, 300, 36, 0.30f);
	p_pxWinSprite->AddFrame(0, 144, 300, 36, 0.30f);
	p_pxWinSprite->AddFrame(0, 180, 300, 36, 0.30f);
	m_pxWinText = new WinText(p_pxWinSprite, 110, -36,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	p_pxWinFrog =
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/EndStateText.bmp", 200, 20, 84, 84);
	m_pxWinFrog = new WinText(p_pxWinFrog, (m_xSystem.m_iScreenWidth / 2) - 42,
		(m_xSystem.m_iScreenHeight / 2) -42,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	p_pxPressEnterSprite = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/EndStateText.bmp");
	p_pxPressEnterSprite->AddFrame(0, 216, 280, 36, 0.30f);
	p_pxPressEnterSprite->AddFrame(0, 252, 280, 36, 0.30f);
	p_pxPressEnterSprite->AddFrame(0, 288, 280, 36, 0.30f);
	m_pxWinText2 = new WinText(p_pxPressEnterSprite, 120, 336,
		m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);

	Mix_PlayMusic(m_xMusic, 1);
}

void WinState::Exit()
{

}

bool WinState::Update(float p_fDeltaTime)
{
	m_pxWinText->Update(p_fDeltaTime);
	m_pxWinText2->Update(p_fDeltaTime);

	p_pxWinSprite->Update(p_fDeltaTime);
	p_pxPressEnterSprite->Update(p_fDeltaTime);
	if (m_xSystem.m_pxInput->IsKeyDownOnce(SDLK_RETURN))
	{
		return false;
	}
	if (m_xSystem.m_pxInput->IsKeyDownOnce(SDLK_ESCAPE))
	{
		SDL_QUIT;
	}

	return true;
}

void WinState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(p_pxWinSprite, m_pxWinText->GetX(), m_pxWinText->GetY());
	m_xSystem.m_pxDrawManager->Draw(p_pxWinFrog, m_pxWinFrog->GetX(), m_pxWinFrog->GetY());
	m_xSystem.m_pxDrawManager->Draw(p_pxPressEnterSprite, m_pxWinText2->GetX(), m_pxWinText2->GetY());
}

IState* WinState::NextState()
{
	return new MenuState(m_xSystem);
}

