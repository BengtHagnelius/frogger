#pragma once

class DrawManager;
class SpriteManager;
class StateManager;
class InputManager;

class Engine
{
public:
	Engine();
	~Engine();
	bool Initialize(); 
	void Shutdown();
	void Update();
	void HandleEvents();


	bool m_bRunning;

private:
	DrawManager* m_pxDrawManager;
	SpriteManager* m_pxSpriteManager;
	StateManager* m_pxStateManager;
	InputManager* m_pxInput;
	
};
