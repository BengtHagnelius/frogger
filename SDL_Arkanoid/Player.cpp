#include "stdafx.h"
#include "Player.h"
#include "Mouse.h"
#include "Sprite.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "InputManager.h"

Player::Player(InputManager* p_pxInput, Sprite* pxPlayerSprite,
	float p_fX, float p_fY, int p_iScreenHeight, int p_iScreenWidth)
{
	m_pxInput = p_pxInput;
	m_pxSprite = pxPlayerSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenHeight = p_iScreenHeight;
	m_iScreenWidth = p_iScreenWidth;
	m_bVisible = true;
	m_pxCollider = new Collider(
		pxPlayerSprite->GetRegion()->w,
		pxPlayerSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
	m_xPlopSound = nullptr;

	m_xPlopSound = Mix_LoadWAV("../assets/Bloop.wav");
	if (m_xPlopSound == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}
}

Player::~Player()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;
}

void Player::Update(float p_fDeltaTime)
{
	if (!m_bEnd && m_bGameStart)
	{
		if (m_pxInput->IsKeyDownOnce(SDLK_w))
		{
			m_fY -= 20;
			m_iWDir = 1;
			Mix_PlayChannel(-1, m_xPlopSound, 0);
		}
		if (m_pxInput->IsKeyDownOnce(SDLK_s))
		{
			m_fY += 20;
			m_iWDir = 2;
			Mix_PlayChannel(-1, m_xPlopSound, 0);
		}
		if (m_pxInput->IsKeyDownOnce(SDLK_a))
		{
			m_fX -= 20;
			m_iWDir = 3;
			Mix_PlayChannel(-1, m_xPlopSound, 0);
		}
		if (m_pxInput->IsKeyDownOnce(SDLK_d))
		{
			m_fX += 20;
			m_iWDir = 4; 
			Mix_PlayChannel(-1, m_xPlopSound, 0);
		}

		if (m_fX < 0)
		{
			m_fX = 0;
		}
		//   posx + bredd > screenwidth
		//   ________. <- that point
		//
		if ((m_fX + 20) > m_iScreenWidth)
		{
			m_fX = m_iScreenWidth - 20;
		}
		if ((m_fY + 20) > m_iScreenHeight-20)
		{
			m_fY = m_iScreenHeight - 40;
		}
	}
	
	

	m_pxCollider->Refresh();
}


Sprite* Player::GetSprite() { return m_pxSprite; }

Collider* Player::GetCollider()
{
	return m_pxCollider;
}

float Player::GetX() { return m_fX; }

float Player::GetY() { return m_fY; }
/*
int Player::WalkingDirection()
{
	if (WalkDown == true)
	{
		return 1;
	}
	else if (WalkLeft == true)
	{
		return 2;
	}
	else if (WalkRight == true)
	{
		return 3;
	}
	else
	{
		return 0;
	}
}*/

void Player::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
	// If we move the player with setposition we need to update the collider to reflect that move.
	m_pxCollider->Refresh();
}

bool Player::IsVisible() { return m_bVisible; }

EENTITYTYPE Player::GetType() { return EENTITYTYPE::ENTITY_PLAYER; }